import React                    from 'react'
import { render }               from 'react-dom'
import { Provider }             from 'react-redux'
import { Router, hashHistory }  from 'react-router'
import routes                   from './routes'
import store                    from './redux/store'

/** render the main component and mount to the root DOM elements
*   Subscribe and make store available to all nested components using Provider
*/
render(
  <Provider store={store}>
    <Router history={hashHistory} routes={routes} />
  </Provider>,
  document.getElementById('react-mount')
)