import React, { PropTypes, Component } from 'react'

// Item.propTypes = {
//   testItems: PropTypes.array
// }

// stateless function component for using internal state or life cycle hook use classes
const Item = ({ testItems }) => {
  return (
    <ul>
      {testItems.map((item, key) => {
        return <li key={key}><h2>{item.text}</h2></li>
      })}
    </ul>
  )
}

export default Item