import React, { PropTypes, Component } from 'react'
import { Link } from 'react-router'
import Category from './Category'
import Item from './Item'
import Items from './Items'
import Nav from './Nav'
import Preview from './Preview'

class StyleCreator extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      departmentid: 0
    }
  }
  
  componentWillMount() {
    let departmentid = this.props.departments[this.props.params.department].id
    this.setState({ departmentid: departmentid })
  }
  
  componentWillReceiveProps() {
    let departmentid = this.props.departments[this.props.params.department].id
    this.setState({ departmentid: departmentid })
  }
  
  render() {
    const department = this.props.params.department
    return (
      <div>
      <h1>this is style creator</h1>
      <h2>this is {}</h2>
      <Nav departments={this.props.departments} />
      <Category />
      <Preview />
      <Items />
      </div>
    )
  }
}

export default StyleCreator

StyleCreator.propTypes = {
  params: PropTypes.object
}