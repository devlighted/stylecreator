import React, { PropTypes, Component } from 'react'
import Item from './Item'

// Items.propTypes = {
// }

// stateless function component for using internal state or life cycle hook use classes
const Items = () => {
  const items = Array.from({length: 5}).map((data) => ({
		text: 'test'
	  })
  )
  
  return (
    <div>
      <h1>Item list</h1>
      <Item testItems={items} />
    </div>
  )
}

export default Items