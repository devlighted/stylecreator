import React, { PropTypes, Component } from 'react'
import { Link }             from 'react-router'

// stateless function component for using internal state or life cycle hook use classes
const Nav = ({ departments }) => {
  return (
    <div>
      <center>Change department</center>
      <ul role="nav">
      {Object.keys(departments).map((department, index) => {
        return <li key={index}><Link to={`/departments/${department}`}>{department}</Link></li>
      })}
      </ul>
    </div>
  )
}

export default Nav

Nav.propTypes = {
  departments: PropTypes.object
}