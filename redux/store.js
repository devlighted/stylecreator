import { applyMiddleware, compose, createStore } from 'redux'
import logger                                    from 'redux-logger'
import thunk                                     from 'redux-thunk'
import reducer                                   from './modules/styleCreator'

let initialState = {
  clothes: {
    snow: 'snow jacket',
    moto: 'moto jacket',
    street: 'street jacket',
    snowmobile: 'snow mobile jacket'
  },
  departments: {
    snow: {
      id: 1,
      name: 'snow'
    },
    streetwear: {
      id: 2,
      name: 'streetwear'
    },
    snowmobile: {
      id: 4,
      name: 'snowmobile'
    },
    motocross: {
      id: 3,
      name: 'motocross'
    }
  }
}

// Configure logger
const loggerMiddleware = logger({
  level: 'info',
  collapsed: true,
})

// Configure middleWare with thunk, logger and time travel debugger tool for chrome
const finalCreateStore = compose(
  applyMiddleware(thunk, loggerMiddleware),
   window.devToolsExtension ? window.devToolsExtension() : f => f
)

// Create store with reducer, state and enhancers
const store = createStore(reducer, initialState, finalCreateStore)

export default store