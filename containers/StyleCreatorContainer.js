import React, { Component } from 'react'
import { connect }  from 'react-redux'
import { bindActionCreators } from 'redux'
import StyleCreator from '../components/StyleCreator'
import {
  getProducts
} from '../redux/modules/styleCreator'

// Making state avaivable as props
function mapStateToProps(state) {
  return {
    departments: state.departments
  }
}

// Wrapping dipatch actions and making actions avaivable as props
function mapDispatchToProps(dispatch) {
  return {
    getProducts: bindActionCreators(getProducts, dispatch)
  }
}

// Injecting state and dispatch to props on component and connects component to redux
export default connect(mapStateToProps, mapDispatchToProps)(StyleCreator)