import React                from 'react'
import { Route, IndexRedirect } from 'react-router'
import {
  StyleCreator,
  Department
} from './containers'

export default (
  <Route path="/" component={StyleCreator}>
    <IndexRedirect to="/departments/snow" />
    <Route path="departments/:department" name="departments" component={StyleCreator}></Route>
  </Route>
)